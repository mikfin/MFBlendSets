**Render settings**

![The alternative text](images/RenderSettings.png)


**Dither settings**

![The alternative text](images/DitherSettings.png)

**Light path settings**

![The alternative text](images/LightPathSettings.jpg)